#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::DelegatorRef<int(int) const>;
}

#define BENCH_NAME delegator_ref
#include "baseline.cpp"
#undef BENCH_NAME

