#include <function2.hpp>

namespace
{
using F = fu2::unique_function<int(int) const>;
}

#define BENCH_NAME fu2_unique_function
#include "baseline.cpp"
#undef BENCH_NAME

