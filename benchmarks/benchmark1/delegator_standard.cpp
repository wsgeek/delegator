#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::StandardDelegator<int(int) const>;
}

#define BENCH_NAME delegator_standard
#include "baseline.cpp"
#undef BENCH_NAME

