#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::StandardDelegator<int(int)>;
// using F = loblib::delegator::SzDelegator<int(int), 64>;
} // namespace

#define BENCH_NAME delegator_standard
#include "baseline.cpp"
#undef BENCH_NAME

