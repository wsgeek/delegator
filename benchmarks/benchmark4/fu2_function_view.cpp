#include <function2.hpp>

namespace
{
using F = fu2::function_view<int(int)>;
}

#define BENCH_NAME fu2_function_view
#include "baseline.cpp"
#undef BENCH_NAME

