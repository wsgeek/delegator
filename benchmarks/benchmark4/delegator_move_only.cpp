#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::MoveOnlyDelegator<int(int)>;
}

#define BENCH_NAME delegator_move_only
#include "baseline.cpp"
#undef BENCH_NAME

