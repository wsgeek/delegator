#include <loblib/delegator.hpp>

namespace
{
struct BigObject;
using F = loblib::delegator::Delegator<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegator_default
#include "baseline.cpp"
#undef BENCH_NAME
