#include <functional>

namespace
{
struct BigObject;
using F = std::move_only_function<int(BigObject&) const>;
} // namespace

#define BENCH_NAME std_move_only
#include "baseline.cpp"
#undef BENCH_NAME

