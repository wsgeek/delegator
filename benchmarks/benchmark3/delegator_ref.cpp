#include <loblib/delegator.hpp>

namespace
{
struct BigObject;
using F = loblib::delegator::DelegatorRef<int(BigObject&) const>;
}

#define BENCH_NAME delegator_ref
#include "baseline.cpp"
#undef BENCH_NAME

