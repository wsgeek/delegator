#include <function2.hpp>

namespace
{
struct BigObject;
using F = fu2::function_view<int(BigObject&) const>;
} // namespace

#define BENCH_NAME fu2_function_view
#include "baseline.cpp"
#undef BENCH_NAME

