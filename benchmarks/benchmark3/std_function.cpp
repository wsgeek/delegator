#include <functional>

namespace
{
struct BigObject;
using F = std::function<int(BigObject&)>;
} // namespace

#define BENCH_NAME std_function
#include "baseline.cpp"
#undef BENCH_NAME
