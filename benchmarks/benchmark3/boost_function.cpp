#include <boost/function.hpp>

namespace
{
struct BigObject;
using F = boost::function<int(BigObject&)>;
} // namespace

#define BENCH_NAME boost_function
#include "baseline.cpp"
#undef BENCH_NAME
