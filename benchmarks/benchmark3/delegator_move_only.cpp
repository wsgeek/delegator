#include <loblib/delegator.hpp>

namespace
{
struct BigObject;
using F = loblib::delegator::MoveOnlyDelegator<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegator_move_only
#include "baseline.cpp"
#undef BENCH_NAME

