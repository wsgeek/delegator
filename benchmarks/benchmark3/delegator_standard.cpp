#include <loblib/delegator.hpp>

namespace
{
struct BigObject;
using F = loblib::delegator::StandardDelegator<int(BigObject&) const>;
} // namespace

#define BENCH_NAME delegator_standard
#include "baseline.cpp"
#undef BENCH_NAME

