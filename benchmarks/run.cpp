#include <nanobench.hpp>

void baseline(ankerl::nanobench::Bench* bench, char const* name);
void std_function(ankerl::nanobench::Bench* bench, char const* name);
void p2548_copyable(ankerl::nanobench::Bench* bench, char const* name);
void boost_function(ankerl::nanobench::Bench* bench, char const* name);
void fu2_function(ankerl::nanobench::Bench* bench, char const* name);
void delegator_default(ankerl::nanobench::Bench* bench, char const* name);
void delegator_standard(ankerl::nanobench::Bench* bench, char const* name);

void std_move_only(ankerl::nanobench::Bench* bench, char const* name);
void fu2_unique_function(ankerl::nanobench::Bench* bench, char const* name);
void delegator_move_only(ankerl::nanobench::Bench* bench, char const* name);

void p2548_function_ref(ankerl::nanobench::Bench* bench, char const* name);
void fu2_function_view(ankerl::nanobench::Bench* bench, char const* name);
void delegator_ref(ankerl::nanobench::Bench* bench, char const* name);

int main() {

	ankerl::nanobench::Bench b;
	b.title("candidate").warmup(50).relative(true).epochs(200);
	b.performanceCounters(true);

	baseline(&b, "baseline");
	std_function(&b, "std_function");
	p2548_copyable(&b, "p2548_copyable");
	boost_function(&b, "boost_function");
	fu2_function(&b, "fu2_function");
	delegator_default(&b, "delegator_default");
	delegator_standard(&b, "delegator_standard");

	std_move_only(&b, "std_move_only");
	fu2_unique_function(&b, "fu2_unique_function");
	delegator_move_only(&b, "delegator_move_only");

	p2548_function_ref(&b, "p2548_function_ref");
	fu2_function_view(&b, "fu2_function_view");
	delegator_ref(&b, "delegator_ref");

	// // this is just a special case for benchmark4
	// fu2_function(&b, "fu2_function64");
	// delegator_standard(&b, "sz_delegator64");

	return 0;
}
