#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::MoveOnlyDelegator<int(int) const>;
}

#define BENCH_NAME delegator_move_only
#include "baseline.cpp"
#undef BENCH_NAME

