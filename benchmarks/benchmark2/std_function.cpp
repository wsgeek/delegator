#include <functional>

namespace
{
using F = std::function<int(int)>;
}

#define BENCH_NAME std_function
#include "baseline.cpp"
#undef BENCH_NAME
