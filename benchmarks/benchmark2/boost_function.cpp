#include <boost/function.hpp>

namespace
{
using F = boost::function<int(int)>;
}

#define BENCH_NAME boost_function
#include "baseline.cpp"
#undef BENCH_NAME
