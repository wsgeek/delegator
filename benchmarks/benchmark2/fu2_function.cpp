#include <function2.hpp>

namespace
{
using F = fu2::function<int(int) const>;
}

#define BENCH_NAME fu2_function
#include "baseline.cpp"
#undef BENCH_NAME

