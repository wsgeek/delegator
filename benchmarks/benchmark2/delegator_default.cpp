#include <loblib/delegator.hpp>

namespace
{
using F = loblib::delegator::Delegator<int(int) const>;
}

#define BENCH_NAME delegator_default
#include "baseline.cpp"
#undef BENCH_NAME
