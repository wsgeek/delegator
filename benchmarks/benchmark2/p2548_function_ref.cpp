#include <p2548_function_ref.hpp>

namespace
{
using F = p2548::function_ref<int(int) const>;
}

#define BENCH_NAME p2548_function_ref
#include "baseline.cpp"
#undef BENCH_NAME
