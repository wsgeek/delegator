#include <p2548_copyable_function.hpp>

namespace
{
using F = p2548::copyable_function<int(int) const>;
}

#define BENCH_NAME p2548_copyable
#include "baseline.cpp"
#undef BENCH_NAME
