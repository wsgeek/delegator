#ifndef LOBLIB_DELEGATOR_ERROR_POLICIES
#define LOBLIB_DELEGATOR_ERROR_POLICIES

#ifndef DEPS_INCLUDED
#	include <cassert>
#	include <stdexcept>
#	include <functional>
#endif // !DEPS_INCLUDED

///////////////////////////////////////////////////////

namespace internal
{
#include "object_manager/error_policies.hpp"
}; // namespace internal

///////////////////////////////////////////////////////

class CheckNone : protected internal::CheckNone
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_call = false;

	static void on_invoke() noexcept {}

	CheckNone() = default;
	~CheckNone() = default;
};

class CheckAndContinue : protected internal::CheckAndContinue
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_call = true;

	static void on_invoke() noexcept {}

	CheckAndContinue() = default;
	~CheckAndContinue() = default;
};

class AssertOnError : protected internal::AssertOnError
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_call = true;

	static void on_invoke() noexcept { assert(false); }

	AssertOnError() = default;
	~AssertOnError() = default;
};

class ThrowOnError : protected internal::ThrowOnError
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_call = true;

	static void on_invoke() { throw std::bad_function_call(); }

	ThrowOnError() = default;
	~ThrowOnError() = default;
};

#endif // !LOBLIB_DELEGATOR_ERROR_POLICIES
