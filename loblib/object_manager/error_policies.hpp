#ifndef LOBLIB_OBJECT_MANAGER_ERROR_POLICIES
#define LOBLIB_OBJECT_MANAGER_ERROR_POLICIES

#ifndef DEPS_INCLUDED
#	include <cassert>
#	include <stdexcept>
#endif // !DEPS_INCLUDED

///////////////////////////////////////////////////////

class CheckNone
{
  protected:
	static constexpr bool check_before_access = false;
	static constexpr bool check_before_copy = false;
	static constexpr bool check_before_destroy = false;

	static void on_access() noexcept {}
	static void on_copy() noexcept {}
	static void on_destroy() noexcept {}

	CheckNone() = default;
	~CheckNone() = default;
};

class CheckAndContinue
{
  protected:
	static constexpr bool check_before_access = true;
	static constexpr bool check_before_copy = true;
	static constexpr bool check_before_destroy = true;

	static void on_access() noexcept {}
	static void on_copy() noexcept {}
	static void on_destroy() noexcept {}

	CheckAndContinue() = default;
	~CheckAndContinue() = default;
};

class AssertOnError : protected CheckAndContinue
{
  protected:
	static void on_access() noexcept { assert(false); }
	static void on_copy() noexcept { assert(false); }
	// 'on_destroy' is empty because assertion on destruction could possibly affect performance

	AssertOnError() = default;
	~AssertOnError() = default;
};

class ThrowOnError : protected CheckAndContinue
{
  protected:
	static void on_access() {
		throw std::logic_error("Attempted to access an invalid object!");
	}
	static void on_copy() {
		throw std::logic_error("Attempted to copy an invalid object!");
	}
	// 'on_destroy' is empty because destruction should not throw!

	ThrowOnError() = default;
	~ThrowOnError() = default;
};

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER_ERROR_POLICIES
