#ifndef LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES
#define LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES

#ifndef DEPS_INCLUDED
#	include <cstddef>
#	include <type_traits>
#	include <utility>
#	include <memory>
#	include <atomic>
#endif // !DEPS_INCLUDED

///////////////////////////////////////////////////////

namespace internal
{

template <typename T, template <typename> class Allocator>
class ReferenceCountingWrapper
{
  public:
	using AllocatorType = Allocator<ReferenceCountingWrapper>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;

  private:
	T _obj;
	std::atomic<int> _count{0};

	friend AllocatorTraits;

  public:
	template <typename... Args>
	[[nodiscard]]
	static ReferenceCountingWrapper*
	create_new(AllocatorType& alloc, Args&&... args) {
		ReferenceCountingWrapper* ptr = AllocatorTraits::allocate(alloc, 1);
		AllocatorTraits::construct(alloc, ptr, std::forward<Args>(args)...);
		return ptr;
	}

	[[nodiscard]]
	static T& access(ReferenceCountingWrapper* target) noexcept {
		return target->_obj;
	}

	[[nodiscard]]
	static ReferenceCountingWrapper* copy(ReferenceCountingWrapper* target) noexcept {
		(target->_count).fetch_add(1, std::memory_order_acq_rel);
		return target;
	}

	static void destroy(AllocatorType& alloc,
	                    ReferenceCountingWrapper* target) noexcept {
		if ((target->_count).fetch_sub(1, std::memory_order_acq_rel) == 1) {
			AllocatorTraits::destroy(alloc, target);
			AllocatorTraits::deallocate(alloc, target, 1);
		}
	}

	// private:
	template <typename... Args>
	ReferenceCountingWrapper(Args&&... args)
		: _obj(std::forward<Args>(args)...) {
		_count.store(1, std::memory_order_release);
	}

	~ReferenceCountingWrapper() = default;
};

} // namespace internal

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class DeepCopy
{
  public:
	using AllocatorType = Allocator<TargetType>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = TargetType*;

	template <typename... Args>
	static constexpr bool can_nothrow_construct = false;

	static constexpr bool can_nothrow_copy = false;

  protected:
	DeepCopy() = default;
	~DeepCopy() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept {
		return *stored;
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args) {
		static_assert(std::is_copy_constructible_v<StoredType>,
		              "Target object must be copy-constructible");

		*target = AllocatorTraits::allocate(alloc, 1);
		AllocatorTraits::construct(alloc, *target, std::forward<Args>(args)...);
	}

	static void copy(AllocatorType& alloc, const StoredType& source,
	                 StoredType* destination) {
		*destination = AllocatorTraits::allocate(alloc, 1);
		AllocatorTraits::construct(alloc, *destination, *source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept {
		AllocatorTraits::destroy(alloc, *target);
		AllocatorTraits::deallocate(alloc, *target, 1);
	}
};

///////////////////////////////////////////////////////

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class ReferenceCounting
{
  private:
	using WrappedTarget = internal::ReferenceCountingWrapper<TargetType, Allocator>;

  public:
	using AllocatorType = WrappedTarget::AllocatorType;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = WrappedTarget*;

	template <typename... Args>
	static constexpr bool can_nothrow_construct = false;

	static constexpr bool can_nothrow_copy = false;

  protected:
	ReferenceCounting() = default;
	~ReferenceCounting() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept {
		return WrappedTarget::access(stored);
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args) {
		static_assert(std::is_copy_constructible_v<TargetType>,
		              "Target object must be copy-constructible");

		*target = WrappedTarget::create_new(alloc, std::forward<Args>(args)...);
	}

	static void copy(AllocatorType&, const StoredType& source,
	                 StoredType* destination) noexcept {
		*destination = WrappedTarget::copy(source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept {
		WrappedTarget::destroy(alloc, *target);
	}
};

///////////////////////////////////////////////////////

template <template <typename, template <typename> class> class,
          template <typename, template <typename> class> class>
class is_same_ownership_policy : public std::false_type
{};

template <template <typename, template <typename> class> class OP>
class is_same_ownership_policy<OP, OP> : public std::true_type
{};

template <template <typename, template <typename> class> class OP1,
          template <typename, template <typename> class> class OP2>
inline constexpr bool is_same_ownership_policy_v =
	is_same_ownership_policy<OP1, OP2>::value;

///////////////////////////////////////////////////////

#endif // !LOBLIB_OBJECT_MANAGER_OWNERSHIP_POLICIES
