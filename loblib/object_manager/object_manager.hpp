#ifndef LOBLIB_OBJECT_MANAGER
#define LOBLIB_OBJECT_MANAGER

#ifndef DEPS_INCLUDED
#	include <cstddef>
#	include <type_traits>
#	include <utility>
#	include <memory>
#	include <algorithm>

#	include <atomic>
#	include <exception>
#	include <cassert>
#	include <stdexcept>

#	define DEPS_INCLUDED
#endif // !DEPS_INCLUDED

namespace loblib::object_manager
{

#include "error_policies.hpp"
#include "ownership_policies.hpp"

namespace internal
{

template <std::size_t Size>
union Any
{
	void* _head = nullptr;
	std::byte _data[std::max<std::size_t>(Size, 1)];

	Any() noexcept {};
	Any(std::nullptr_t) noexcept {};
	Any& operator=(std::nullptr_t) noexcept {
		_head = nullptr;
		return *this;
	}

	[[nodiscard]]
	void* address() noexcept {
		return &_data[0];
	}

	[[nodiscard]]
	const void* address() const noexcept {
		return &_data[0];
	}

	[[nodiscard]]
	void*& head() noexcept {
		return _head;
	}

	[[nodiscard]]
	void* const& head() const noexcept {
		return _head;
	}

	template <typename T>
	requires std::is_const_v<std::remove_reference_t<T>>
	[[nodiscard]]
	T interpret_as() const noexcept {
		static_assert(sizeof(T) <= Size,
		              "'Any' is smaller than the requested object!");
		using Stored = std::decay_t<T>;
		return std::forward<T>(*static_cast<const Stored*>(address()));
	}

	template <typename T>
	[[nodiscard]]
	T interpret_as() noexcept {
		// implementation in terms of const version
		using CT = const std::remove_reference_t<T>&;
		return const_cast<T&&>((static_cast<const Any&>(*this)).interpret_as<CT>());
	}

	template <typename T>
	static constexpr bool can_construct() {
		return (sizeof(T) <= Size) && (alignof(T) <= alignof(void*))
		       && (alignof(void*) % alignof(T) == 0);
	}
};

template <typename TargetType, template <typename> class Allocator>
requires std::same_as<TargetType, std::decay_t<TargetType>>
class LocalOwnership
{
  public:
	using AllocatorType = Allocator<TargetType>;
	using AllocatorTraits = std::allocator_traits<AllocatorType>;
	using StoredType = std::decay_t<TargetType>;

	template <typename... Args>
	static constexpr bool can_nothrow_construct =
		std::is_nothrow_constructible_v<StoredType, Args...>;

	static constexpr bool can_nothrow_copy =
		can_nothrow_construct<const StoredType&>;

  protected:
	LocalOwnership() = default;
	~LocalOwnership() = default;

	static const TargetType& get_target(const StoredType& stored) noexcept {
		return stored;
	}

	template <typename... Args>
	static void create(AllocatorType& alloc, StoredType* target, Args&&... args)
	noexcept(can_nothrow_construct<Args...>)
	{
		AllocatorTraits::construct(alloc, target, std::forward<Args>(args)...);
	}

	static void
	copy(AllocatorType& alloc, const StoredType& source, StoredType* destination)
	noexcept(can_nothrow_copy)
	{
		AllocatorTraits::construct(alloc, destination, source);
	}

	static void destroy(AllocatorType& alloc, StoredType* target) noexcept {
		AllocatorTraits::destroy(alloc, target);
	}
};

template 
<
	typename TargetType, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	template <typename> class Allocator
>
class ManagerHelper
	: public std::conditional_t <
				internal::Any<BufferSize>::template can_construct<TargetType>(), 
				internal::LocalOwnership<TargetType, Allocator>, 
				OwnershipPolicy<TargetType, Allocator>
			 >
{
  protected:
	using MaxBuffer = internal::Any<BufferSize>;

	static constexpr bool using_soo =
		MaxBuffer::template can_construct<TargetType>();

	using OwnPol = std::conditional_t <
						using_soo, 
						internal::LocalOwnership<TargetType, Allocator>, 
						OwnershipPolicy<TargetType, Allocator>
					>;

  public:
	using AllocatorType = OwnPol::AllocatorType;
	using StoredType = OwnPol::StoredType;
	using Buffer = internal::Any<sizeof(StoredType)>;

	template <typename... Args>
	static constexpr bool is_constructible =
		Buffer::template can_construct<StoredType>()
		&& std::is_constructible_v<std::decay_t<TargetType>, Args...>;

	static constexpr bool is_copy_constructible =
		Buffer::template can_construct<StoredType>()
		&& std::is_copy_constructible_v<std::decay_t<TargetType>>;
};

} // namespace internal

template 
<
	typename TargetType, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	class ErrorPolicy, 
	std::size_t BufferSize, 
	template <typename> class Allocator
>
class ObjectManager
	: private internal::ManagerHelper <
					TargetType, 
					OwnershipPolicy, 
					BufferSize, 
					Allocator
				>::AllocatorType
	, private internal::ManagerHelper <
					TargetType, 
					OwnershipPolicy, 
					BufferSize, 
					Allocator
				>
	, public ErrorPolicy
{

  private:
	using ManagerHelper = internal::ManagerHelper <
					TargetType, 
					OwnershipPolicy, 
					BufferSize, 
					Allocator
				>;

	using typename ManagerHelper::OwnPol;
	using typename ManagerHelper::AllocatorType;
	using typename ManagerHelper::StoredType;
	using typename ManagerHelper::Buffer;

	Buffer _local_buffer;

	////////////////////////

	[[nodiscard]]
	AllocatorType& _allocator() noexcept {
		return static_cast<AllocatorType&>(*this);
	}

	[[nodiscard]]
	const StoredType& _stored_object() const noexcept {
		return _local_buffer.template interpret_as<const StoredType&>();
	}

	[[nodiscard]]
	StoredType* _stored_object_ptr() noexcept {
		return static_cast<StoredType*>(_local_buffer.address());
	}

	static constexpr bool _can_nothrow_access() noexcept {
		if constexpr (ErrorPolicy::check_before_access) {
			return noexcept(ErrorPolicy::on_access());
		}
		return true;
	}

	////////////////////////

  public:
	[[nodiscard]]
	const TargetType& access() const& 
	noexcept(_can_nothrow_access()) 
	{
		if constexpr (ErrorPolicy::check_before_access) {
			if (is_invalid()) {
				ErrorPolicy::on_access();
				std::terminate();
			}
		}

		return OwnPol::get_target(_stored_object());
	}

	[[nodiscard]]
	TargetType& access() &
	noexcept(_can_nothrow_access()) 
	{
		return const_cast<TargetType&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	[[nodiscard]]
	const TargetType&& access() const&& 
	noexcept(_can_nothrow_access()) 
	{
		return static_cast<const TargetType&&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	[[nodiscard]]
	TargetType&& access() && 
	noexcept(_can_nothrow_access()) 
	{
		return const_cast<TargetType&&>(
			static_cast<const ObjectManager&>(*this).access());
	}

	////////////////////////

	using ManagerHelper::using_soo;

	// general constructor
	template <typename... Args>
	requires(!((std::is_same_v<ObjectManager, std::remove_cvref_t<Args>> && ...)
	           && (sizeof...(Args) == 1)))
	ObjectManager(Args&&... args)
	noexcept(OwnPol::template can_nothrow_construct<Args...>)
	requires(ManagerHelper::template is_constructible<Args...>)
		: AllocatorType() {
		static_assert(sizeof(typename ManagerHelper::MaxBuffer) >= sizeof(Buffer),
		              "Buffer size is too small for the specified target "
		              "object and the ownership policy!");

		OwnPol::create(_allocator(), _stored_object_ptr(),
		               std::forward<Args>(args)...);
	}

	// copy constructor
	ObjectManager(const ObjectManager& other)
	noexcept(noexcept(ErrorPolicy::on_copy()) 
	         && OwnPol::can_nothrow_copy)
	requires(ManagerHelper::is_copy_constructible)
		: AllocatorType(other) {

		if constexpr (ErrorPolicy::check_before_copy) {
			if (other.is_invalid()) {
				ErrorPolicy::on_copy();
				invalidate();
				return;
			}
		}

		OwnPol::copy(_allocator(), other._stored_object(), _stored_object_ptr());
	}

	// move constructor
	ObjectManager(ObjectManager&& other) noexcept
		: AllocatorType(other)
		, _local_buffer(std::move(other._local_buffer)) 
	{ other.invalidate(); }

	// copy assignment operator
	ObjectManager& operator=(const ObjectManager& other)
	noexcept(noexcept(ErrorPolicy::on_copy())
	         && OwnPol::can_nothrow_copy)
	{
		ObjectManager{other}.swap(*this);
		return *this;
	}

	~ObjectManager() {
		if constexpr (ErrorPolicy::check_before_destroy) {
			if (is_invalid()) {
				ErrorPolicy::on_destroy();
				return;
			}
		}

		OwnPol::destroy(_allocator(), _stored_object_ptr());
	}

	// move assignment operator
	ObjectManager& operator=(ObjectManager&& other) noexcept {
		if constexpr (ErrorPolicy::check_before_destroy) {
			if (is_invalid()) { ErrorPolicy::on_destroy(); }
			else { OwnPol::destroy(_allocator(), _stored_object_ptr()); }
		}
		else { OwnPol::destroy(_allocator(), _stored_object_ptr()); }

		_local_buffer = std::move(other._local_buffer);
		other.invalidate();

		return *this;
	}

	void swap(ObjectManager& other) noexcept {
		Buffer tmp_buffer{nullptr};

		tmp_buffer = std::move(other._local_buffer);
		other._local_buffer = std::move(_local_buffer);
		_local_buffer = std::move(tmp_buffer);
	}

	////////////////////////

	void invalidate() noexcept {
		// setting head to nullptr won't work because target object can have
		// first sizeof(void*) bytes set to zero but still be valid
		_local_buffer.head() = _local_buffer.address();
	}

	[[nodiscard]]
	bool is_invalid() const noexcept {
		return _local_buffer.head() == _local_buffer.address();
	}
};

///////////////////////////////////////////////////////

} // namespace loblib::object_manager

#endif // !LOBLIB_OBJECT_MANAGER
