#ifndef LOBLIB_DELEGATOR_HANDLERS
#define LOBLIB_DELEGATOR_HANDLERS

#ifndef DEPS_INCLUDED
#	include <cstddef>
#	include <type_traits>
#	include <functional>

#	include <utility>
#	include <memory>
#	include <algorithm>

#	include <atomic>
#	include <exception>
#	include <cassert>
#	include <stdexcept>
#endif // !DEPS_INCLUDED

///////////////////////////////////////////////////////

namespace internal
{

#ifndef LOBLIB_OBJECT_MANAGER_ERROR_POLICIES
#	include "object_manager/error_policies.hpp"
#endif // !LOBLIB_OBJECT_MANAGER_ERROR_POLICIES

#include "object_manager/object_manager.hpp"

template <typename Signature>
class SimpleHandlerVTable;

template <typename Signature>
class DestructibleHandlerVTable;

template <typename Signature>
class CloneableHandlerVTable;

template 
<
	typename Signature, 
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator
>
class VTableHandler;

///////////////////////////////////////////////////////

template <typename T>
constexpr bool make_false() {
	return false;
}

class UndefinedClass;
inline constexpr std::size_t MPtrSize = sizeof(void(UndefinedClass::*)());

template <int Target>
static constexpr std::size_t AlignTo(std::size_t N) {
	if (N == 0) { return 0; }
	return Target * ((N - 1) / Target + 1);
}

// change to 'is_invocable_r', 'is_nothrow_invocable_r' that are available since cpp23
template <typename T, typename ReturnType, bool Noexcept, typename... ParamTypes>
using is_invocable = std::conditional_t <
							Noexcept, 
							std::is_nothrow_invocable<T, ParamTypes...>, 
							std::is_invocable<T, ParamTypes...>
						>;

template <typename T>
inline constexpr bool is_dereferencable_v =
	requires { typename std::pointer_traits<T>::element_type; };

template <typename T>
using is_dereferencable = std::bool_constant<is_dereferencable_v<T>>;

template <typename T>
requires is_dereferencable_v<T>
using Dereferenced = typename std::pointer_traits<T>::element_type;

///////////////////////////////////////////////////////

#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "handlers_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "handlers_impl.hpp"

} // namespace internal

#endif // !LOBLIB_DELEGATOR_HANDLERS
