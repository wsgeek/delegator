#ifndef LOBLIB_DELEGATOR

#	include <cstddef>
#	include <type_traits>
#	include <utility>
#	include <functional>

template <class Storage>
class DelegatorBase;

template 
<
	typename Signature,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy,
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy,
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator
>
class Delegator;

template <typename T>
inline constexpr bool is_function_pointer_v = false;

template <typename>
inline constexpr bool is_in_place_type_v = false;

template <typename>
inline constexpr bool is_delegator_v = false;

#endif // !LOBLIB_DELEGATOR

///////////////////////////////////////////////////////

#ifndef _LOBLIBCXX_FTOR_CV
#	define _LOBLIBCXX_FTOR_CV
#endif

#ifdef _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF
#else
#	define _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV&
#endif

#define _LOBLIBCXX_FTOR_CV_REF _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF

template 
<
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy,
	template <typename, template <typename> class> 
	class OwnershipPolicy,
	template <typename> 
	class AcceptancePolicy,
	class ErrorPolicy, 
	std::size_t BufferSize,
	template <typename> 
	class Allocator,
	typename ReturnType,
	bool Noexcept,
	typename... ParamTypes
>
class Delegator
	<
		ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept), 
		StoragePolicy, 
		OwnershipPolicy, 
		AcceptancePolicy, 
		ErrorPolicy, 
		BufferSize, 
		Allocator
	>
	: private DelegatorBase <
			StoragePolicy
			<
				ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept),
				OwnershipPolicy, 
				AcceptancePolicy, 
				ErrorPolicy, 
				BufferSize, 
				Allocator
			> >
{
  private:
	using Signature = ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept);
	using Base = DelegatorBase <
					StoragePolicy
					<
						Signature, 
						OwnershipPolicy, 
						AcceptancePolicy, 
						ErrorPolicy, 
						BufferSize, 
						Allocator
					> >;

	using Storage = Base::StorageType;
	using Base::_local_buffer;

	template <typename Fun, typename... Args>
	static constexpr bool _check_nothrow() noexcept {
		return Storage::template Traits<Fun>::template is_nothrow_constructible<Args...>;
	}

  public:
	Delegator() = delete;
	Delegator(const Delegator& other) = default;
	Delegator(Delegator&& other) noexcept = default;
	Delegator& operator=(const Delegator&) = default;
	Delegator& operator=(Delegator&&) noexcept = default;

	Delegator(std::nullptr_t) noexcept : Base(nullptr) {}
	Delegator& operator=(std::nullptr_t) noexcept {
		Base::operator=(nullptr);
		return *this;
	}

	// allow users to check if their target object would be constructed locally
	template <typename Fun>
	static constexpr bool using_soo = Storage::template Traits<Fun>::using_soo;

	// general constructor
	template <typename Fun, typename DFun = std::decay_t<Fun>>
	requires(!std::is_same_v<DFun, Delegator>) && (!is_in_place_type_v<DFun>)
	Delegator(Fun&& fun) 
	noexcept(_check_nothrow<DFun, Fun>()) 
	{
		if constexpr (is_function_pointer_v<std::remove_cvref_t<Fun>>
		              || std::is_member_pointer_v<DFun> || is_delegator_v<DFun>) {
			if (fun == nullptr) { return; }
		}

		Storage::template create<Fun>(_local_buffer, std::forward<Fun>(fun));
	}

	// to allow in-place construction
	template <typename Fun, typename... Args>
	requires std::is_constructible_v<std::decay_t<Fun>, Args...>
	explicit Delegator(std::in_place_type_t<Fun>, Args&&... args) 
	noexcept(_check_nothrow<Fun, Args...>()) 
	{
		static_assert(std::is_same_v<std::decay_t<Fun>, Fun>);
		Storage::template create<Fun>(_local_buffer, std::forward<Args>(args)...);
	}

	// to allow in-place construction with an initializer list
	template <typename Fun, typename T, typename... Args>
	requires std::is_constructible_v<Fun, std::initializer_list<T>&, Args...>
	explicit Delegator
		(
			std::in_place_type_t<Fun>, 
			std::initializer_list<T> in_list,
			Args&&... args
		) 
	noexcept(_check_nothrow<Fun, std::initializer_list<T>&, Args...>()) 
	{
		static_assert(std::is_same_v<std::decay_t<Fun>, Fun>);
		Storage::template create<Fun>(_local_buffer, in_list,
		                              std::forward<Args>(args)...);
	}

	// to allow changing the target object
	template <typename Fun>
	requires std::is_constructible_v<Delegator, Fun>
	Delegator& operator=(Fun&& fun) 
	noexcept(_check_nothrow<std::decay_t<Fun>, Fun>()) 
	{
		Delegator{std::forward<Fun>(fun)}.swap(*this);
		return *this;
	}

	ReturnType operator()(ParamTypes... params) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) {

		if constexpr (Storage::ErrorHandler::check_before_call) {
			if (Storage::is_empty(_local_buffer)) {
				Storage::ErrorHandler::on_invoke();
			}
		}

		using TpInv = Storage::Buffer _LOBLIBCXX_FTOR_INV_QUALS;
		return std::invoke(Storage::access(std::forward<TpInv>(_local_buffer)),
		                   std::forward<ParamTypes>(params)...);
	}

	void swap(Delegator& other) noexcept { Base::swap(other); }

	friend void swap(Delegator& x, Delegator& y) noexcept { x.swap(y); }

	friend bool operator==(const Delegator& x, std::nullptr_t) noexcept {
		return Storage::is_empty(x._local_buffer);
	}
};

#undef _LOBLIBCXX_FTOR_CV_REF
#undef _LOBLIBCXX_FTOR_CV
#undef _LOBLIBCXX_FTOR_REF
#undef _LOBLIBCXX_FTOR_INV_QUALS

///////////////////////////////////////////////////////

