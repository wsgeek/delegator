#ifndef LOBLIB_DELEGATOR_ACCEPTANCE_POLICIES
#define LOBLIB_DELEGATOR_ACCEPTANCE_POLICIES

#ifndef DEPS_INCLUDED
#	include <type_traits>
#	include <memory>
#endif // !DEPS_INCLUDED

#include "handlers.hpp"

///////////////////////////////////////////////////////

template <typename Signature>
class StandardAccept;

template <typename Signature>
class NonCopyableAccept;

template <typename Signature>
class PtrAccept;

template <typename Signature>
class NonCopyablePtrAccept;

///////////////////////////////////////////////////////

#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "acceptance_policies_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "acceptance_policies_impl.hpp"

#endif // !LOBLIB_DELEGATOR_ACCEPTANCE_POLICIES
