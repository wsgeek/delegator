#ifndef LOBLIB_DELEGATOR
#define LOBLIB_DELEGATOR

#ifndef DEPS_INCLUDED
#	include <cstddef>
#	include <type_traits>
#	include <functional>

#	include <utility>
#	include <memory>
#	include <exception>
#	include <algorithm>

#	include <atomic>
#	include <cassert>
#	include <stdexcept>

#	define DEPS_INCLUDED
#endif // !DEPS_INCLUDED

namespace loblib::delegator
{

#include "error_policies.hpp"
#include "object_manager/ownership_policies.hpp"
#include "storage_policies.hpp"
#include "acceptance_policies.hpp"

///////////////////////////////////////////////////////

template <class Storage>
class DelegatorBase : public Storage
{
  protected:
	using StorageType = Storage;
	Storage::Buffer _local_buffer{nullptr};

	DelegatorBase() noexcept = default;

	~DelegatorBase() { Storage::destroy(_local_buffer); }

	// copy ctor
	DelegatorBase(const DelegatorBase& other) {
		Storage::copy(other._local_buffer, _local_buffer);
	}

	// move ctor
	DelegatorBase(DelegatorBase&& other) noexcept
		: _local_buffer(std::move(other._local_buffer)) {
		Storage::set_to_empty(other._local_buffer);
	}

	// construct as empty
	DelegatorBase(std::nullptr_t) noexcept {
		Storage::set_to_empty(_local_buffer);
	}

	// copy assignment operator
	DelegatorBase& operator=(const DelegatorBase& other) {
		DelegatorBase{other}.swap(*this);
		return *this;
	}

	// move assignment operator
	DelegatorBase& operator=(DelegatorBase&& other) noexcept {
		Storage::destroy(_local_buffer);
		_local_buffer = std::move(other._local_buffer);
		Storage::set_to_empty(other._local_buffer);

		return *this;
	}

	// assignment to empty
	DelegatorBase& operator=(std::nullptr_t) noexcept {
		Storage::destroy(_local_buffer);
		Storage::set_to_empty(_local_buffer);

		return *this;
	}

	void swap(DelegatorBase& other) noexcept {
		typename Storage::Buffer tmp_buffer{nullptr};

		tmp_buffer = std::move(other._local_buffer);
		other._local_buffer = std::move(_local_buffer);
		_local_buffer = std::move(tmp_buffer);
	}
};

///////////////////////////////////////////////////////

template 
<
	typename Signature,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class StoragePolicy = OwningStorage,
	template <typename, template <typename> class> 
	class OwnershipPolicy = ReferenceCounting,
	template <typename> 
	class AcceptancePolicy = StandardAccept,
	class ErrorPolicy = AssertOnError, 
	std::size_t BufferSize = sizeof(void*),
	template <typename> 
	class Allocator = std::allocator
>
class Delegator;

template <typename Signature>
using MoveOnlyDelegator =
	Delegator<Signature, OwningStorage, DeepCopy, NonCopyableAccept>;

template <typename Signature>
using DelegatorRef =
	Delegator<Signature, RefOnlyStorage, LocalOwnership, StandardAccept>;

template <typename Signature>
using StandardDelegator =
	Delegator<Signature, OwningStorage, DeepCopy, 
              StandardAccept, ThrowOnError, internal::MPtrSize>;

template <typename Signature, std::size_t BufferSize>
using SzDelegator = Delegator<Signature, OwningStorage, ReferenceCounting,
                              StandardAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzMoveOnlyDelegator =
	Delegator<Signature, OwningStorage, DeepCopy, NonCopyableAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzDelegatorRef = Delegator<Signature, RefOnlyStorage, LocalOwnership,
                                 StandardAccept, AssertOnError, BufferSize>;

template <typename Signature, std::size_t BufferSize>
using SzStandardDelegator =
	Delegator<Signature, OwningStorage, DeepCopy, StandardAccept, ThrowOnError, BufferSize>;

///////////////////////////////////////////////////////

template <typename T>
inline constexpr bool is_function_pointer_v =
	std::is_function_v<typename std::remove_pointer_t<T>> && std::is_pointer_v<T>;

template <typename T>
using is_function_pointer = std::bool_constant<is_function_pointer_v<T>>;

template <typename>
inline constexpr bool is_in_place_type_v = false;

template <typename T>
inline constexpr bool is_in_place_type_v<std::in_place_type_t<T>> = true;

template <typename T>
using is_in_place_type = std::bool_constant<is_in_place_type_v<T>>;

///////////////////////////////////////////////////////

template <typename>
inline constexpr bool is_delegator_v = false;

template 
<
	typename Sgn,
	template 
	<
		typename, 
		template <typename, template <typename> class> class, 
		template <typename> class, 
		class, 
		std::size_t, 
		template <typename> class
	>
	class Storg,
	template <typename, template <typename> class> 
	class Ownsp,
	template <typename> 
	class Acc,
	class Err, 
	std::size_t S,
	template <typename> 
	class Alloc
>
inline constexpr bool is_delegator_v<Delegator<Sgn, Storg, Ownsp, Acc, Err, S, Alloc>> =
	true;

template <typename T>
using is_delegator = std::bool_constant<is_delegator_v<T>>;

///////////////////////////////////////////////////////

#include "delegator_impl.hpp"
#define _LOBLIBCXX_FTOR_CV const
#include "delegator_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &
#include "delegator_impl.hpp"
#define _LOBLIBCXX_FTOR_REF &&
#include "delegator_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &
#include "delegator_impl.hpp"
#define _LOBLIBCXX_FTOR_CV  const
#define _LOBLIBCXX_FTOR_REF &&
#include "delegator_impl.hpp"

} // namespace loblib::delegator

#endif // !LOBLIB_DELEGATOR
