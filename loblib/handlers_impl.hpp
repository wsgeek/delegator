#ifndef LOBLIB_DELEGATOR_HANDLERS

#	include <cstddef>
#	include <type_traits>
#	include <functional>

#	include "object_manager/error_policies.hpp"
#	include "object_manager/object_manager.hpp"

template <typename Signature>
class SimpleHandlerVTable;

template <typename Signature>
class DestructibleHandlerVTable;

template <typename Signature>
class CloneableHandlerVTable;

template 
<
	typename Signature, 
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator
>
class VTableHandler;

template <typename T>
constexpr bool make_false() {
	return false;
}

// change to 'is_invocable_r', 'is_nothrow_invocable_r' that are available since cpp23
template <typename T, typename ReturnType, bool Noexcept, typename... ParamTypes>
using is_invocable = std::conditional_t <
							Noexcept, 
							std::is_nothrow_invocable<T, ParamTypes...>, 
							std::is_invocable<T, ParamTypes...>
						>;

template <typename T>
inline constexpr bool is_dereferencable_v =
	requires { typename std::pointer_traits<T>::element_type; };

template <typename T>
using is_dereferencable = std::bool_constant<is_dereferencable_v<T>>;

template <typename T>
requires is_dereferencable_v<T>
using Dereferenced = typename std::pointer_traits<T>::element_type;

#endif // !LOBLIB_DELEGATOR_HANDLERS

///////////////////////////////////////////////////////

#ifndef _LOBLIBCXX_FTOR_CV
#	define _LOBLIBCXX_FTOR_CV
#endif

#ifdef _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF
#else
#	define _LOBLIBCXX_FTOR_REF
#	define _LOBLIBCXX_FTOR_INV_QUALS _LOBLIBCXX_FTOR_CV&
#endif

#define _LOBLIBCXX_FTOR_CV_REF _LOBLIBCXX_FTOR_CV _LOBLIBCXX_FTOR_REF

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class SimpleHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  protected:
	template <typename T>
	using RefNonTrivials = std::conditional_t<std::is_scalar_v<T>, T, T&&>;

  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

  protected:
	~SimpleHandlerVTable() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class DestructibleHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  protected:
	template <typename T>
	using RefNonTrivials = std::conditional_t<std::is_scalar_v<T>, T, T&&>;

  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

	virtual ~DestructibleHandlerVTable() = default;
};

template <typename ReturnType, bool Noexcept, typename... ParamTypes>
class CloneableHandlerVTable
		<ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept)>
{
  protected:
	template <typename T>
	using RefNonTrivials = std::conditional_t<std::is_scalar_v<T>, T, T&&>;

  public:
	virtual ReturnType operator()(RefNonTrivials<ParamTypes>...) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) = 0;

	virtual void clone_itself(void* destination) const = 0;
	virtual ~CloneableHandlerVTable() = default;
};

template 
<
	typename InvocableType, 
	class VTable, 
	template <typename, template <typename> class> 
	class OwnershipPolicy, 
	std::size_t BufferSize, 
	bool AcceptPointers, 
	template <typename> 
	class Allocator,
	typename ReturnType,
	bool Noexcept,
	typename... ParamTypes
>
class VTableHandler <
			ReturnType(ParamTypes...) _LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept), 
			InvocableType, 
			VTable, 
			OwnershipPolicy, 
			BufferSize, 
			AcceptPointers, 
			Allocator
		>
	final : public VTable
{
  private:
	using Manager = loblib::object_manager::ObjectManager <
						InvocableType, 
						OwnershipPolicy, 
						CheckNone, 
						BufferSize, 
						Allocator
					>;
	Manager _manager;

  public:
	static constexpr bool using_soo = Manager::using_soo;

	template <typename... Args>
	requires
	(std::is_constructible_v<std::decay_t<InvocableType>, Args...>)
	&& (!
	( 
		(std::is_same_v<VTableHandler, std::remove_cvref_t<Args>> && ...)
		&& (sizeof...(Args) == 1)
	))
	VTableHandler(Args&&... args)
	noexcept( std::is_nothrow_constructible_v<Manager, Args...> )
		: _manager(std::forward<Args>(args)...) {}

	VTableHandler(const VTableHandler& other) = default;
	VTableHandler(VTableHandler&& other) noexcept = delete;

	VTableHandler& operator=(const VTableHandler&) = delete;
	VTableHandler& operator=(VTableHandler&&) noexcept = delete;

	~VTableHandler() = default;

	// this one is not marked 'final' because 'clone_itself' might not be in the vtable
	void clone_itself(void* destination) const {
		::new (destination) VTableHandler(*this);
	}

	ReturnType operator()(VTable::template RefNonTrivials<ParamTypes>... params) 
		_LOBLIBCXX_FTOR_CV_REF noexcept(Noexcept) final {

		using FunInvQuals = InvocableType _LOBLIBCXX_FTOR_INV_QUALS;

		if constexpr (
			is_invocable<FunInvQuals, ReturnType, Noexcept, ParamTypes...>::value) {
			return std::invoke(
				std::forward<FunInvQuals>(_manager.access()),
				std::forward<typename VTable::template RefNonTrivials<ParamTypes>>(
					params)...);
		}
		else if constexpr (AcceptPointers && is_dereferencable_v<InvocableType>) {
			using DereferencedInvQuals =
				Dereferenced<InvocableType> _LOBLIBCXX_FTOR_INV_QUALS;

			if constexpr (is_invocable<DereferencedInvQuals, ReturnType,
			                           Noexcept, ParamTypes...>::value) {
				return std::invoke(
					std::forward<DereferencedInvQuals>(*_manager.access()),
					std::forward<typename VTable::template RefNonTrivials<ParamTypes>>(
						params)...);
			}
			else {
				static_assert(make_false<InvocableType>(),
				              "Target does not point to an invocable object!");
			}
		}
		else {
			static_assert(make_false<InvocableType>(),
			              "Target is not invocable!");
		}
	}
};

#undef _LOBLIBCXX_FTOR_CV_REF
#undef _LOBLIBCXX_FTOR_CV
#undef _LOBLIBCXX_FTOR_REF
#undef _LOBLIBCXX_FTOR_INV_QUALS

///////////////////////////////////////////////////////

