# `loblib::delegator`

The biggest of all thanks to [Andrei Alexandrescu](http://erdani.org/index.html).  
Without his work, this would not be possible, nor would I even decide to learn C++.

> **DISCLAIMER**  
This software is in its initial stages and has not undergone extensive testing. As such, there are probably some bugs and possibly even inefficiencies. Users are advised to exercise caution, conduct their own testing and report a bug if they encounter one. The latter would be very appreciated. This disclaimer will hopefully be removed soon.

## What is this?

The `loblib::delegator` is a C++ header-only template utility that is meant to provide a superior alternative to `std::function` and company, without sacrificing anything.

## Why superior to `std::function`?

- is `const`, `reference` and `noexcept` correct, i.e. includes qualifiers as a part of the signature (this bug of `std::function` is detailed in N4348, is meant to be fixed by `std::copyable_function` (C++26) and is already fixed by `std::move_only_function`)
- has resizable local buffer, which makes it easy to avoid (or to entirely forbid) allocating on the heap
- allows an easy switch to your custom allocator
- allows in-place construction of the target object, by using `std::in_place_type` (this is also implemented by `std::move_only_function`)
- can be made to accept pointers (and pointer-like objects) to invocables (this can be useful, for example, if you want a safe version of `std::function_ref` or if you already have a pointer to the object you want to call and want to avoid unnecessary dereferencing and copying)
- is more performant: it fixes some bugs that `std::function` has (for example, `std::function` passes built-in types by reference), it allows for reference counting instead of deep-copying, it utilizes gcc's ability to partially inline vtable pointers, it is RTTI-free and it can be exception-free
- is extremely flexible: by changing template arguments one can have different behaviors (behaviors of `std::function`, `std::move_only_function`, and `std::function_ref` are all supported as well as many others that can be tailored to your needs)
- is easy to use: you do not have to remember all the different names like 'function', 'move_only_function', 'function_ref', 'copyable_function' ... and you do not have to remember all
the differences between them because API communicates all the differences to you
- is easily modifiable because of strong separation of concerns (if you change error handling behavior, for example, everything else is guaranteed to work as before)

And all of this without any sacrifices in performance, safety, robustness, readability or ease of use!

## How is this even possible?

It's simple: because of the policy-based design!
For each part of the behavior, you specify a policy. Every combination of policies gives you a different behavior.
Most common behaviors are covered by policies that are provided, but if some of them do not fit your bill, all you have to do is write your own (or modify an existing one); everything else stays the same! And all the policies are small and simple, without any data members, so it usually boils down to writing a few static member functions!

Apart from the custom allocator, `Delegator` has 4 policies:

- **Storage policies** specify what will and how be stored in the local storage.
    - `OwningStorage` will always have its own copy of the target object (either through pointer to free memory or in its local buffer on the stack).
    - `NonOwningStorage` will store a reference to the target object instead of the target object itself only if the target is an lvalue reference or if small object optimization can not be used. Therefore, it is not always non-owning.
    - `RefOnlyStorage` will only ever store references, even if you give it an rvalue reference (this makes it ideal for function parameter types).
    - `NonManagingStorage` will only accept trivially-constructible target objects that can be constructed in its local buffer on the stack. This is ideal when you want to make sure you do not pay for copying/destroying objects that do not need to be copied/destroyed in any special way.

- **Ownership policies** specify the ownership strategy.
    - `LocalOwnership` is the strategy that is always used for target objects that can be constructed locally (i.e. it is used for small object optimization - SOO). If specified as the template argument, target objects will never be constructed on the heap. If some other storage policy is specified, `LocalOwnership` will still be used for SOO. If you want to disable SOO, set `BufferSize` to zero. `NonManagingStorage` and `RefOnlyStorage` work only with this strategy, since they prohibit resource management.  
    - `DeepCopy` will always copy the target object. This can be expensive if the target is expensive to copy and/or is stored on the heap.
    - `ReferenceCounting` will never copy the target object, but will instead increase the reference count. The count is stored together with the target object so that there is no memory overhead or memory fragmentation. The count is thread-safe and lock-free.

- **Acceptance policies** specify which target objects can be accepted.
    - `StandardAccept` resembles the acceptance strategy of `std::function`. Non-copyable invocables and pointers to invocables are not accepted.
    - `PtrAccept` is the same as `StandardAccept` but pointers to invocables are accepted.
    - `NonCopyableAccept` is the same as `StandardAccept` but non-copyable invocables are accepted. This implies that copying is prohibited.
    - `NonCopyablePtrAccept` is the same as `NonCopyableAccept` but pointers to invocables are accepted.

- **Error policies** specify what to do when trying to copy/destroy/access or invoke an invalid `Delegator`.
    - `CheckNone` performs no checking and therefore no error-handling. This can be used in performance-critical code to have one `if` less, but use with caution.
    - `CheckAndContinue` performs a check before copy/destroy and invoke, but does no error-handling (only returns and continues).
    - `AssertOnError` performs a check before specified actions and asserts that `Delegator` is in a valid state.
    - `ThrowOnError` performs a check before specified actions and throws if `Delegator` is in an invalid state. On invoke, `std::bad_function_call()` is thrown and on copy/destroy/access `std::logic_error` is thrown.

In addition to those 4 policies, you can specify two additional template arguments.  
- `BufferSize` specifies the size threshold for using SOO: objects bigger than this size will never be constructed locally on the stack. If set to 0, SOO will be disabled. By default, it is equal to `sizeof(void*)`. Make sure you increase this if you are using it for pointers to member functions or other bigger objects!
- `Allocator` specifies allocator to be used for (de)allocation and construction/destruction. It needs to be compatible with `std::allocator_traits`

All the policy template arguments have default policies, so you can always choose to be ignorant.  
And there are also a few predefined type-aliases, so that you can be even more lazy!

## Great! How do I start using it?

The `loblib::delegator` has a very permissive MIT license, so no worries there.  
It has no dependencies other than the standard library, so no worries there.  
It is header-only, so all you have to do is copy-paste. There are actually multiple '.hpp' files, but that is purely for organizational purposes (one could merge them in a single big header file, but that would be impractical).  

Therefore, all you have to do is copy a directory named 'loblib' to your project and include 'delegator.hpp' that is inside. That is all!  

> **NOTE**  
The minimum required C++ version is C++20.  
If you want to run the benchmarks yourself, you will need C++23 and boost.

## Examples, please!

```cpp
#include "loblib/delegator.hpp"
using namespace loblib::delegator;

int some_func(int x) { return 2 * x; }

int main() {

	// you can simply use it like this ...
	Delegator<int(int)> cmd1{some_func};

	// which is equivalent to ...
	Delegator<int(int), OwningStorage, ReferenceCounting, StandardAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd1_eq{some_func};

	// if you want behaviour more similar to 'std::function' ...
	StandardDelegator<int(int)> cmd2{some_func};

	// which is equivalent to ...
	Delegator<int(int), OwningStorage, DeepCopy, StandardAccept,
	          ThrowOnError, 2 * sizeof(void*), std::allocator> cmd2_eq{some_func};

	// if you want behaviour similar to 'std::move_only_function' ...
	MoveOnlyDelegator<int(int)> cmd3{some_func};

	// which is equivalent to ...
	Delegator<int(int), OwningStorage, DeepCopy, NonCopyableAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd3_eq{some_func};

	// and if you want behaviour similar to 'std::function_ref' ...
	DelegatorRef<int(int)> cmd4{some_func};

	// which is equivalent to ...
	Delegator<int(int), RefOnlyStorage, LocalOwnership, StandardAccept,
	          AssertOnError, sizeof(void*), std::allocator> cmd4_eq{some_func};

	// you can also use each version with prefix 'Sz' that stands for 'Sizeable' ...
	SzDelegator<int(int), 2 * sizeof(void*)> cmd5{some_func};

	// or another example ...
	SzMoveOnlyDelegator<int(int), 64> cmd6{some_func};

	// you can also check if an object is of a 'Delegator' type ...
	static_assert(is_delegator_v<decltype(cmd1)>);

	// and you can check if your invocable object would be stored locally or on the heap!
	static_assert(Delegator<int(int)>::using_soo<decltype(some_func)>);

	return 0;
}
```

```cpp
#include "loblib/delegator.hpp"
using namespace loblib::delegator;

struct StatefulFunctor
{
	long n{};
	long m{};
	StatefulFunctor(long x, long y) : n(x), m(y) {}

	long operator()() const&& noexcept { return n * m; }
};

// make sure you set the size of the local buffer to avoid allocating on the heap
using F = SzDelegator<long() const && noexcept, sizeof(StatefulFunctor)>;

#include <iostream>

int main() {

	// construct `StatefulFunctor` in place
	const F cmd{std::in_place_type<StatefulFunctor>, 42, 3};

	// this would not work since 'operator()' has 'const&&' in its signature
	// std::cout << cmd() << std::endl;

	// this will print 126
	std::cout << std::move(cmd)() << '\n';

	return 0;
}
```

## And you claim I get all of this without sacrificing performance?

The `loblib::delegator` is carefully designed so that the overhead compared to calling the invocable object directly is almost zero. Except for one thing: the type-erasure that lies at the heart of functionality that `std::function` provides makes it hard for compilers to inline the call. Compilers will never inline a call through a function pointer, but they might inline a call through a vtable pointer that they created, using a technique called devirtualization. The `loblib::delegator` gives all the hints to compilers that they are free to devirtualize, but there is no guarantee they will do it. If they do, you can get a truly zero overhead delegator! As of now, gcc is able to do this, although partially and not always. The compilers will most likely improve in the future in this respect, so `loblib::delegator` is expected to be even faster!

> **NOTE**  
Make sure you do **not** have `-fno-devirtualize` among your compiler flags, as disabling devirtualization will degrade performance!

#

Here are a few simple benchmarks performed using [nanobench](https://github.com/martinus/nanobench).  

> **DISCLAIMER**  
This benchmarking is by no means comprehensive and should be used only as a guideline.  
If somebody is willing to do a more complete one, I would be happy to include it here (either through link or directly). You can contact me by the e-mail provided below.  
All the benchmarking was done using g++ 13.2 with `-O3 -std=c++23 -fno-lto -fno-whole-program`. Other compilers were not tested!

### Benchmark1

baseline : calling a stateless lambda N times

```cpp
void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	auto lambda = [](int x) { return x * 3; };
	using BaselineType = decltype(lambda);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{lambda};

	bench->run(name,
	           [&]()
	           {
				   int accumulator = 1;
				   for (size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |        4,960,861.00 |   30,000,019.00 |   20,004,359.00 |  10,000,005.00 |      0.99 |
| `std_function`       |    22.2% |       22,319,243.50 |  120,000,036.00 |   90,020,400.50 |  40,000,022.00 |      4.46 |
| `p2548_copyable`     |    40.0% |       12,402,179.00 |   90,000,026.00 |   50,011,674.00 |  30,000,012.00 |      2.48 |
| `boost_function`     |    33.3% |       14,879,304.00 |  120,000,029.00 |   60,013,615.50 |  40,000,015.00 |      2.98 |
| `fu2_function`       |    40.0% |       12,397,245.00 |   90,000,026.00 |   50,011,611.00 |  30,000,012.00 |      2.48 |
| `delegator_default`  |   100.1% |        4,958,237.00 |   40,000,030.00 |   20,004,365.00 |  10,000,008.00 |      0.99 |
| `delegator_standard` |   100.0% |        4,960,811.00 |   40,000,030.00 |   20,004,368.50 |  10,000,008.00 |      0.99 |
| `std_move_only`      |    40.0% |       12,398,086.50 |   80,000,026.00 |   50,011,555.50 |  30,000,012.00 |      2.48 |
| `fu2_unique_function`|    33.3% |       14,879,870.00 |   90,000,029.00 |   60,013,363.50 |  30,000,015.00 |      2.98 |
| `delegator_move_only`|   100.1% |        4,958,006.50 |   40,000,030.00 |   20,004,355.00 |  10,000,008.00 |      0.99 |
| `p2548_function_ref` |    33.3% |       14,876,543.50 |   80,000,029.00 |   60,013,378.50 |  30,000,015.00 |      2.98 |
| `fu2_function_view`  |    40.0% |       12,395,992.50 |   90,000,026.00 |   50,011,620.00 |  30,000,012.00 |      2.48 |
| `delegator_ref`      |   100.0% |        4,960,912.00 |   40,000,030.00 |   20,004,381.00 |  10,000,008.00 |      0.99 |

### Benchmark2

baseline : calling a function pointer N times

```cpp
namespace { int testfunc(int x) { return x * 3; } }

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = int (*)(int);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   int accumulator = 1;
				   for (size_t i = 0; i < N; i++) {
					   accumulator = std::invoke(cmd, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       12,398,973.00 |   70,000,026.00 |   50,011,585.00 |  30,000,012.00 |      2.48 |
| `std_function`       |    53.8% |       23,048,974.00 |  140,000,037.00 |   92,944,306.00 |  50,000,023.00 |      4.60 |
| `p2548_copyable`     |    71.4% |       17,356,463.50 |  120,000,031.00 |   70,016,646.50 |  40,000,017.00 |      3.47 |
| `boost_function`     |    71.4% |       17,360,977.00 |  150,000,031.00 |   70,017,060.00 |  50,000,017.00 |      3.47 |
| `fu2_function`       |    62.5% |       19,843,988.00 |  210,000,034.00 |   80,019,475.50 |  60,000,020.00 |      3.97 |
| `delegator_default`  |    83.3% |       14,883,271.50 |  130,000,029.00 |   60,013,409.50 |  50,000,016.00 |      2.98 |
| `delegator_standard` |    83.3% |       14,880,831.50 |  130,000,029.00 |   60,013,583.00 |  50,000,016.00 |      2.98 |
| `std_move_only`      |    83.3% |       14,882,900.50 |  110,000,029.00 |   60,013,595.50 |  40,000,015.00 |      2.98 |
| `fu2_unique_function`|    71.4% |       17,364,343.50 |  210,000,031.00 |   70,016,939.50 |  60,000,017.00 |      3.47 |
| `delegator_move_only`|   100.0% |       12,401,302.50 |  130,000,026.00 |   50,011,820.00 |  50,000,013.00 |      2.48 |
| `p2548_function_ref` |    71.4% |       17,363,953.00 |  110,000,031.00 |   70,016,634.00 |  40,000,017.00 |      3.47 |
| `fu2_function_view`  |    83.3% |       14,882,745.50 |  120,000,029.00 |   60,013,623.00 |  40,000,015.00 |      2.98 |
| `delegator_ref`      |    83.3% |       14,883,787.00 |  130,000,029.00 |   60,013,606.00 |  50,000,016.00 |      2.98 |

### Benchmark3

baseline : calling a function that has a big object parameter N times

```cpp
namespace {

struct BigObject
{
	char data[64];
	int n{};

	BigObject(int x) : n(x){};
};

int testfunc(BigObject x) { return x.n * 3; }

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;
	using BaselineType = int (*)(BigObject);

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;
	Ftor cmd{testfunc};

	bench->run(name,
	           [&]()
	           {
				   BigObject bo{1};
				   for (size_t i = 0; i < N; i++) {
					   bo.n = std::invoke(cmd, bo);
				   }
				   ankerl::nanobench::doNotOptimizeAway(bo);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       17,359,394.00 |  210,000,041.00 |   70,020,610.00 |  30,000,017.00 |      3.47 |
| `std_function`       |    42.8% |       40,594,607.00 |  260,000,064.00 |  163,739,089.50 |  60,000,041.00 |      8.12 |
| `p2548_copyable`     |    42.7% |       40,645,662.50 |  250,000,064.00 |  163,952,674.00 |  50,000,041.00 |      8.13 |
| `boost_function`     |    43.0% |       40,409,285.00 |  280,000,063.00 |  162,946,294.00 |  60,000,040.00 |      8.08 |
| `fu2_function`       |    42.2% |       41,122,429.00 |  340,000,064.00 |  165,887,717.50 |  70,000,041.00 |      8.21 |
| `delegator_default`  |   100.0% |       17,364,358.50 |  230,000,041.00 |   70,021,124.50 |  40,000,017.00 |      3.47 |
| `delegator_standard` |   100.0% |       17,366,538.00 |  230,000,041.00 |   70,021,583.50 |  40,000,017.00 |      3.47 |
| `std_move_only`      |    42.9% |       40,457,114.00 |  240,000,063.00 |  163,177,275.50 |  50,000,040.00 |      8.08 |
| `fu2_unique_function`|    42.2% |       41,178,033.00 |  340,000,064.00 |  166,055,952.00 |  70,000,041.00 |      8.23 |
| `delegator_move_only`|   100.0% |       17,362,425.00 |  230,000,041.00 |   70,021,123.50 |  40,000,017.00 |      3.47 |
| `p2548_function_ref` |    71.4% |       24,296,182.50 |  240,000,047.00 |   98,031,413.50 |  50,000,024.00 |      4.85 |
| `fu2_function_view`  |    42.9% |       40,498,822.50 |  250,000,063.00 |  163,364,726.00 |  50,000,040.00 |      8.10 |
| `delegator_ref`      |   100.0% |       17,365,705.50 |  230,000,041.00 |   70,021,277.00 |  40,000,017.00 |      3.47 |

### Benchmark4

baseline : calling N stateful functors

```cpp
namespace
{

struct StatefulFunctor
{
	int n = 3;
	int a[15];
	int operator()(int x) { return x * n; }
};

} // namespace

void BENCH_NAME(ankerl::nanobench::Bench* bench, char const* name) {

	static constexpr std::size_t N = 10'000'000;

	StatefulFunctor stateful_functor{};
	using BaselineType = StatefulFunctor;

	using Ftor = std::conditional_t<std::is_same_v<F, void>, BaselineType, F>;

	auto ftors = std::vector<Ftor>{};
	ftors.reserve(N);

	for (std::size_t i = 0; i < N; ++i) {
		ftors.emplace_back(stateful_functor);
	}

	bench->run(name,
	           [&]()
	           {
				   int accumulator = 1;
				   for (auto& ftor : ftors) {
					   accumulator = std::invoke(ftor, accumulator);
				   }
				   ankerl::nanobench::doNotOptimizeAway(accumulator);
			   });
}
```

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `baseline`           |   100.0% |       29,731,116.00 |   25,000,055.00 |  119,120,065.00 |   5,000,035.00 |      5.95 |
| `std_function`       |    67.7% |       43,891,251.00 |  130,000,067.00 |  175,753,212.50 |  40,000,050.00 |      8.78 |
| `p2548_copyable`     |    62.9% |       47,283,695.00 |  110,000,071.00 |  189,314,471.50 |  30,000,054.00 |      9.46 |
| `boost_function`     |    60.7% |       48,943,366.00 |  140,000,073.00 |  195,971,542.50 |  40,000,056.00 |      9.79 |
| `fu2_function`       |    64.7% |       45,966,420.00 |  110,000,070.00 |  184,068,496.50 |  30,000,053.00 |      9.19 |
| `delegator_default`  |    74.7% |       39,785,101.50 |  110,000,062.00 |  159,307,677.00 |  30,000,046.00 |      7.96 |
| `delegator_standard` |    70.1% |       42,413,275.50 |  110,000,065.00 |  169,770,694.50 |  30,000,049.00 |      8.48 |
| `std_move_only`      |    59.9% |       49,628,333.00 |  100,000,074.00 |  198,661,771.50 |  30,000,057.00 |      9.92 |
| `fu2_unique_function`|    59.8% |       49,714,679.50 |  110,000,074.00 |  199,044,545.00 |  30,000,057.00 |      9.92 |
| `delegator_move_only`|    74.0% |       40,202,443.00 |  110,000,062.00 |  160,935,968.00 |  30,000,046.00 |      8.04 |
| `p2548_function_ref` |   185.4% |       16,033,341.50 |   90,000,036.00 |   64,230,411.50 |  30,000,019.00 |      3.21 |
| `fu2_function_view`  |   185.7% |       16,014,300.00 |  110,000,036.00 |   64,242,983.50 |  30,000,019.00 |      3.21 |
| `delegator_ref`      |   278.0% |       10,695,280.50 |  110,000,029.00 |   42,866,836.00 |  30,000,013.00 |      2.14 |

And if the local buffer size is increased to 64 bytes:

| candidate            | relative |               ns/op |          ins/op |          cyc/op |         bra/op |     total |
|:---------------------|---------:|--------------------:|----------------:|----------------:|---------------:|----------:|
| `fu2_function64`     |    72.8% |       41,672,033.00 |  190,000,060.00 |  167,427,223.00 |  50,000,043.00 |      8.34 |
| `sz_delegator64`     |    99.5% |       30,517,848.50 |  100,000,048.00 |  122,584,159.00 |  30,000,032.00 |      6.10 |

<br/><br/>
If you want to run the benchmarks yourself, they are in the directory 'benchmarks'.  

For example, to compile and run the first benchmark:
```bash
cd ./benchmarks
make run_benchmark1.out && ./run_benchmark1.out
# ...
make clean
```

> **NOTE**  
To provide a fair benchmarking, all the candidates are compiled separately and then linked.  
Link-time and whole-program optimizations are turned off with `-fno-lto -fno-whole-program`.

## Contribute

There are a lot of things that can be improved/added to the project. Most notably, unit tests and more complete benchmarks.  
Pull requests and new issues are very welcome!  
You can contact me by e-mail: <lobel.krivic@proton.me>.  

#

I work for no company and have no sponsors.  
If you like my work, consider buying me a cup of coffee...  

<a href="https://buymeacoffee.com/freedom1947" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>  

<a href="https://pay-link.s3.us-west-2.amazonaws.com/index.html?uid=b00668d46dde4925"><img src="https://web.archive.org/web/20130429152428if_/http://www.ecogex.com/bitcoin/pack/ribbonDonateBitcoin.png" height="41" width="174"></a>

## License
[MIT](https://gitlab.com/loblib/delegator/-/blob/master/LICENSE)

